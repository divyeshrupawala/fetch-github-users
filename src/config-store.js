import { createStore } from 'redux'
import appReducer from './index-reducer'

const initialState = {    
    home : {
        isSearch : false,
        hintList :[]
    },
    userDetails : {}
};

const store = createStore(appReducer, initialState);

export default store