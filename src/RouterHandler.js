import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "./home/Home";
import UserDetails from "./user-details/UserDetails"

const RouterHandler = () => {
    return (
    <BrowserRouter>
      <div>        
        <center>        
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/home" component={Home} />
              <Route path="/userDetails" component={UserDetails} />
            </Switch>
        </center>        
      </div>
    </BrowserRouter>
  );
};

export default RouterHandler;
