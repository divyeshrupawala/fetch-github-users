import C from './user-details-constants'

const userDetails = (state=[], action) => {
	switch(action.type) {
		case C.SET_USER_DEATILS:
			return {...action.payload}
		default:
			return state
	}
}

export default userDetails