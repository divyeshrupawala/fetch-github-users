import React from 'react'

import { connect } from "react-redux";
import { withRouter } from "react-router";

const UserDetails = ({userDetails, history}) => {

    const onBack = () => {
        history.push('/home');
    }

    return(
       <div>
           <button onClick={onBack}>Back</button><br/><br/>
            { userDetails.avatar_url ? 
                (<div className="user-details">                    
                    <div><img src={userDetails.avatar_url} width="50px" alt="user icon"/></div>
                    <div><label>Name : </label>{userDetails.login}</div>
                    <div><label>Location : </label>{userDetails.location}</div>
                    <div><label>Number of Public Gists : </label>{userDetails.public_gists}</div>
                    <div><label>Number of Followers : </label><a href={userDetails.followers_url}>{userDetails.followers}</a></div>
                    <div><label>Number of Following : </label>{userDetails.following}</div>
                    <div><label>Profile creation date : </label>{userDetails.created_at}</div>
                 </div>) : 'No Data Found'}
        </div>       
    )
}

const mapStateToProps = (state, props) => {    
    return {
        userDetails : state.userDetails,
        history : props.history
    }
};

const mapDispatchToProps = dispatch => ({
    // onHintListData(data) {
    //     dispatch(hintListData(data));
    // }
});

const container = connect(mapStateToProps, mapDispatchToProps)(UserDetails);

export default withRouter(container);