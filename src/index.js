import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import store from "./config-store";
import { Provider } from "react-redux";
import RouterHandler from "./RouterHandler";

ReactDOM.render(
    <Provider store={store}>
         <RouterHandler />
    </Provider>, document.getElementById('root'));
registerServiceWorker();
