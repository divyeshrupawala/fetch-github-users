import C from './home-constants'
import userDetailConstants from '../user-details/user-details-constants'

export const updateSearch = (isSearch) => 
	({
		type :C.UPDATE_SEARCH,
		payload : isSearch 
	})

export const setUserDetails = (obj) => 
	({
		type : userDetailConstants.SET_USER_DEATILS,
		payload : obj 
	})	

export const hintListData = (data) => 
	({
		type :C.HINT_LIST,
		payload : data 
	})