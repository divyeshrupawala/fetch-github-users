import React from 'react';

import { connect } from "react-redux";

const HintList = ({hintList}) => {
    return (
        <div className="hintList">
            {hintList.length ? (
                hintList.map(value=> {
                return <div key={value.login} style={{margin:'5px'}}>{value.login}</div>
            })) : ''}
        </div>
    )    
}

const mapStateToProps = (state, props) => {
	return {
		hintList : state.home.hintList
	}
}

export default connect(mapStateToProps)(HintList);

