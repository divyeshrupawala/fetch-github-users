import C from './home-constants'

const home = (state=[], action) => {
	switch(action.type) {		
		case C.UPDATE_SEARCH:
			return {...state,isSearch : action.payload};	
		case C.HINT_LIST:
			return {...state, hintList : [...action.payload]}
		default:
			return state
	}
}

export default home