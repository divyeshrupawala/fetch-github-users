import React from 'react'

import { setUserDetails } from './home-actions'

import { connect } from "react-redux";
import { withRouter } from "react-router";

const TabularData = ({ onSetUserDetails = f=>f, isSearch, hintList, history}) => {      

    const getUserDetails = (url) => {
        fetch(url)
        .then( (response) => response)      
        .then((response) => response.json())
        .then( (data) => {            
            onSetUserDetails(data);
            history.push('/userDetails');
        })
        .catch( (error) => console.log('error'+error));
    }
   
    return (
      <div>        
        { (isSearch && hintList.length > 0) ?
             (
                <table className="blueTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Avatar</th>
                            <th>User Login</th>
                            <th>Type</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        {hintList.map(value => {
                            return (
                                <tr key={value.id} style={{ cursor : 'pointer'}} onClick={event => getUserDetails(value.url)}>
                                    <td>{value.id}</td>
                                    <td><img src={value.avatar_url} width="50px" alt="user icon"/></td>
                                    <td>{value.login}</td>
                                    <td>{value.type}</td>
                                    <td>{value.score}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            ): ''}
    </div>
    )    
}

const mapStateToProps = (state, props) => {
    return {
        isSearch : state.home.isSearch,
        hintList : state.home.hintList,
        history : props.history
    }
};

const mapDispatchToProps = dispatch => ({   
    onSetUserDetails(obj) {
        dispatch(setUserDetails(obj));
    }
});

const container = connect(mapStateToProps, mapDispatchToProps)(TabularData);

export default withRouter(container);
