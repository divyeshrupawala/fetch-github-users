import React from 'react'

import HintList from './HintList'
import TabularData from './TabularData'

import { updateSearch, hintListData } from './home-actions'

import { connect } from "react-redux";
import { withRouter } from "react-router";

const Home = ({ onHintListData = f=>f ,onSearch =f=>f }) => {
    let sort = "followers";
    let order = "asc";
    
    const onInputType = (event) => {
        const query = event.target.value;

        fetch(`https://api.github.com/search/users?q=${query}&sort=${sort}&order=${order}&page=1&per_page=10`)
        .then((response) => response)      
        .then((response) => response.json())
        .then((data) => onHintListData(data.items))
        .catch( (error) => console.log('error'+error));
    }

    const onSortChange = (event) => {
        sort = event.target.value;
    }

    const onOrderChange = (event) => {
        order = event.target.value;
    }

    const search = () => {        
        onSearch(true)
    }
    
    return (
      <div>
        <input type="text" name="searchbox" onChange={event => onInputType(event)}/>

        <select onChange={event => onSortChange(event)}>
            <option>followers</option>
            <option>repositories</option>
            <option>joined</option>
        </select>
        <input type="radio" onChange={event => onOrderChange(event)} name="radio1" value="asc"/> Asc
        <input type="radio" onChange={event => onOrderChange(event)} name="radio1" value="desc"/> Desc
        <button onClick={event => search()}>Search</button>
        <HintList />
        <TabularData/>
    </div>
    )    
}

const mapStateToProps = (state, props) => {
    return {

    }
};

const mapDispatchToProps = dispatch => ({
    onHintListData(data) {
        dispatch(hintListData(data));
    },
    onSearch(isSearch) {
        dispatch(updateSearch(isSearch));
    }
});

const container = connect(mapStateToProps, mapDispatchToProps)(Home);

export default withRouter(container);
