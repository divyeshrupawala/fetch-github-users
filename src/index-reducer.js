import { combineReducers } from 'redux'
import home from './home/home-reducer'
import userDetails from './user-details/user-details-reducer'

export default combineReducers({home, userDetails});
